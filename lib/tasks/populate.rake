namespace :db do
  desc "Erase and fill database"
  task :populate => :environment do
    require 'populator'
    require 'faker'
    
    [Author, Book, Reader, BookAuthor, Provider, LendingBook].each(&:delete_all)
    

    #Autor
    Author.populate 10 do |author|
      author.name = Faker::Name.name
      author.nationality = Faker::Address.country
    end
    
    #Lector
    Reader.populate 10 do |reader|
      reader.name    = Faker::Name.name
      reader.email   = Faker::Internet.email
      reader.phone   = Faker::PhoneNumber.phone_number
      reader.address  = Faker::Address.street_address
    end

    #Proveedor
    Provider.populate 10 do |provider|
      provider.name    = Faker::Company.name
      provider.email   = Faker::Internet.email
      provider.phone   = Faker::PhoneNumber.phone_number
      provider.address  = Faker::Address.street_address
    end

    #Libro
    Book.populate 20 do |book|
      book.code = Array(100..999)
      book.title = Populator.words(1..5).titleize
      book.edition = Array(1..15)
      book.description = Populator.sentences(2..10)
      book.quantity = [2, 4, 10]
      book.observation = Populator.sentences(2..10)
      book.provider_id = Array(1..10)
    end


    #Libro-autor
    BookAuthor.populate 10 do |book_author|
      book_author.author_id = Array(1..10)
      book_author.book_id = Array(1..20)
    end

    #Préstamo
    LendingBook.populate 10 do |lending_book|
      lending_book.book_id = Array(1..20)
      lending_book.state = [0, 1]
      lending_book.days = Array(1..10)
      lending_book.amount = [1000, 2000, 3000]
      lending_book.date = 15.days.ago..Time.now
      lending_book.reader_id = Array(1..10)
      lending_book.fine = 0
      lending_book.observation = Populator.sentences(2..10)
    end

    News.populate 5 do |news|
       news.title = Populator.words(1..5).titleize
       news.body = Populator.sentences(2..10)
    end
  end
end

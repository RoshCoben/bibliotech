class ActiveAdmin::Views::Pages::Base < Arbre::HTML::Document

  private

  # Renders the content for the footer
  def build_footer
    div :id => "footer" do
      para "Bibliotech Copyright &copy; #{Date.today.year.to_s} Grupo 3.".html_safe
    end
  end

end
class Reader < ActiveRecord::Base
  attr_accessible :address, :name, :observation, :email, :phone
  validates :name, :address, :presence => true
  has_many :lending_books
end

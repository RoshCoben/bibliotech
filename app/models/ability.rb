class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:

    #Possible actions: create, :read, :update, :destroy
    user ||= AdminUser.new #invitado
 
    
    #can :read, BookAuthor
    can :read, ActiveAdmin::Page, :name => "Dashboard"
    
    can :read, Book
    can :read, Author
     
    # An admin can do the following:
    if user.role=='admin'
        can :manage, :all
    end
     
    # A reader can do the following:
    if user.role=='reader'

        #Manage his own account
        can :read, AdminUser, AdminUser.where(:id => user.id ) do |u|
            u.id == user.id
        end

        can :update, AdminUser, AdminUser.where(:id => user.id ) do |u|
            u.id == user.id
        end

        can :destroy, AdminUser, AdminUser.where(:id => user.id ) do |u|
            u.id == user.id
        end 


        #See his lendings       
        begin
            @reader = (Reader.all(:select => "id", :conditions => ["email = ?", user.email]))[0].id
            can :read, LendingBook, LendingBook.where(:reader_id => @reader ) do |lending_book|
              @reader==lending_book.reader_id
            end
        rescue Exception=>e
            can :read, LendingBook, LendingBook.where(:reader_id => 0 ) do |lending_book|
              0==lending_book.reader_id
            end
        end
        
        
        #See books and its authors
        
        

         
    end
    
  end
end

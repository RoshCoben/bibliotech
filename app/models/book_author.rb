class BookAuthor < ActiveRecord::Base
  attr_accessible :author_id, :book_id
  validates :author_id, :book_id, :presence => true
  belongs_to :author
  belongs_to :book
end

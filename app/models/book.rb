class Book < ActiveRecord::Base
  attr_accessible :code, :title, :edition, :description, :quantity, :observation, :provider_id, :stock
  validates :title, :edition, :quantity, :presence => true
  validates :quantity, :edition, :numericality => { :only_integer => true }
  validates :quantity, :numericality => { :greater_than => 0 }
  validates :edition, :numericality => { :greater_than => 0 }
  has_many :book_authors
  has_many :authors, through: :book_authors

  has_many :lending_books
end

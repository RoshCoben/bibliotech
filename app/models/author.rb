class Author < ActiveRecord::Base
  attr_accessible :name, :nationality
  validates :name, :presence => true
  has_many :book_authors
  has_many :books, through: :book_authors
end

class Provider < ActiveRecord::Base
  attr_accessible :address, :email, :name, :phone
  validates :address, :email, :name, :phone, :presence => true
  validates :email, :format => { :with => /\A(([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,}))?\z/i,
    :message => "no tiene formato valido ejemplo@tecweb.cl" }
  validates :phone, :format => { :with => /^-?((?:\d+|\d*)$)/,
    :message => "debe ingresar un numero valido" } 
     

end

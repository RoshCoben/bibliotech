#encoding: utf-8
class MyValidator < ActiveModel::Validator
  def validate(lending_book)
  	#Cantidad de ese libro = 2
  	book = Book.where(:id => lending_book.book_id).first
    book_stock = book.quantity 
    #Cantidad de prestamos hechos para ese libro
    book_lendings_quantity = LendingBook.where(:book_id => book.id).length 
    if book_lendings_quantity == book_stock 
       lending_book.errors[:book_id] << "No está en stock."
    end
    
  end
end



class LendingBook < ActiveRecord::Base
  include ActiveModel::Validations
  validates_with MyValidator

  attr_accessible :book_id, :state, :days, :amount, :date, :reader_id, :observation, :return, :fine
  validates :reader_id,:book_id, :date, :state, :days, :presence => true
  validates :days, :numericality => { :only_integer => true }
  belongs_to :book
  belongs_to :reader

  scope :Prestados, where(:state => 0)
  scope :En_estante, where(:state => 1)
  scope :Recepcionados, where(:state => 2)
  scope :Todos

end



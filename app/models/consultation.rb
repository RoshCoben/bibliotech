class Consultation < ActiveRecord::Base
  attr_accessible :comment, :email, :name, :subject, :surname
  validates :comment, :email, :name, :subject, :surname, :presence => true
  validates :email, :format => { :with => /\A(([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,}))?\z/i,
    :message => "no tiene formato valido ejemplo@tecweb.cl" }
end

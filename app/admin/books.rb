#encoding: utf-8
ActiveAdmin.register Book do
  config.per_page = 15
  
  menu :label => "Libros"

  sidebar :Ayuda do
    "Envíanos un E-Mail a biblioteca.crco@gmail.com"
  end

  filter :code
  filter :title
  filter :edition
  filter :quantity
  filter :description
  #filter :provider_id
  filter :observation
  
  index :download_links => false, :title => 'Libros' do
  	column :code, :sortable => :code do |book|
  		div :class => "right-align" do
  			book.code
  		end
  	end
  	column :title
    column :edition do |book|
      div :class => "center-align" do
        book.edition
      end
    end
    column :quantity do |book|
      div :class => "center-align" do
        book.quantity
      end
    end
  	default_actions
  end

  #_form
  form do |f|
    f.inputs "Detalles de Libro" do
      f.input :code
      f.input :title
      f.input :edition      
      f.input :quantity
      f.input :description
      f.input :provider_id, :as => :select , :prompt => "- seleccione un productor -", :collection => Provider.all.map { |p| [p.name, p.id] }      
      f.input :observation, :label => 'Observación'
      f.input :stock, :as => :hidden
    end

    f.inputs  do
      "<div id=\"element-name\">
        (*) Campos obligatorios
      </div>".html_safe
    end

    f.actions

  end

  

  #show
  show do |book|
      attributes_table do
        row :code
        row :title
        row :edition
        row :description
        row :quantity
        row :stock do |book|
          #restar los prestados
          prestados = LendingBook.find(:all, :conditions => { :state => 0, :book_id => book.id }).length
          #prestados = LendingBook.where(:id => book.id , :state => 0).length
          book.stock = book.quantity - prestados
        end
        row :observation
        row "Proveedor" do |book|
          if book.provider_id != nil
            provider = Provider.find(book.provider_id)
            link_to provider.name, [:admin, provider]
          end
        end
      end
      q = BookAuthor.where(:book_id => book.id).length
      if q>0 
        table_for BookAuthor.where(:book_id => book.id) do |book_author|
          column "Autor(es)" do |book_author|
            author = Author.find(book_author.author_id)
            link_to author.name, [:admin, author]
          end
        end
      end
  end

end

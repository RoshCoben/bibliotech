#encoding: utf-8
ActiveAdmin.register News do
  config.per_page = 15
  menu :label => "Noticias"

  filter :title

 index :download_links => false, :title => 'Noticias' do
  	column "Titulo", :title
    column "Cuerpo", :body
  	default_actions
  end  

  #_form
  form do |f|
    f.inputs "Detalles de Noticia" do
        f.input :title, :label => 'Titulo'
        f.input :body, :label => 'Cuerpo', as: :html_editor


    end

    f.inputs  do
      "<div id=\"element-name\">
        (*) Campos obligatorios
      </div>".html_safe
    end

    f.actions

  end

  #show
  #show
  show do |news|
      attributes_table do
        row :title
        row :body
      end
      #active_admin_comments
  end
  
end
#encoding: utf-8
ActiveAdmin.register_page "Dashboard" do
  #config.sort_order = "updated_at_desc"

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do

    panel "Actividad reciente" do
        if current_admin_user.role == 'admin'
            table_for LendingBook.order("updated_at desc").limit(10) do
              column "Última modificación", :updated_at
            column "Libro" do |lending_book|
                book = Book.find(lending_book.book_id)
                link_to book.title, [:admin, book]
            end
            column "Estado", :sortable => :state do |lending_book|
                if lending_book.state == 0
                    status_tag("Prestado", :red)
                elsif lending_book.state == 1
                    status_tag("En estante", :green)
                else
                    status_tag("Recepcionado", :orange)
                end
            end
                column "Fecha de préstamo", :date                
                column "Días de préstamo", :days do |lb|
                    div :class => "center-align" do
                        lb.days
                    end
                end
                column "Multa" do |lending_book|
                  if lending_book.state == 0
                    #calcular dias habiles
                    days_after = 0
                    lend = lending_book.date
                    now = Time.current()
                    while now > lend
                     days_after = days_after + 1 unless now.saturday? or now.sunday?
                     now = now - 1.day
                    end

                    if days_after > lending_book.days
                      #tiene multa
                      valor = (((days_after - lending_book.days) * lending_book.amount))
                      fine = number_to_currency(valor, :locale => :es)
                      div :class => "right-align" do
                        status_tag(fine, :red)
                      end
                    else
                      if days_after > lending_book.days / 2
                        div :class => "right-align" do
                            status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :orange)
                        end
                      else
                        div :class => "right-align" do
                            status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :green)
                        end
                      end
                    end
                  else
                    div :class => "right-align" do
                      fine = lending_book.fine
                      number_to_currency(fine, :locale => :es)
                    end
                  end
                end
                if current_admin_user.role == 'admin'
                  column "Entregar libro" do |lending_book|
                    if lending_book.state == 0
                      link_to  "Devolver", "/admin/lending_books/"+lending_book.id.to_s+"/return"
                    end
                  end
                end
                column "Préstamo", :state do |lending_book|
                link_to "Ver préstamo", [:admin, lending_book]
                end
            end

            strong { link_to "Ver Todos", admin_lending_books_path }
        else
            begin
                @reader = (Reader.all(:select => "id", :conditions => ["email = ?", current_admin_user.email]))[0].id
                table_for LendingBook.where(:reader_id => @reader ).order("updated_at desc").limit(10) do            
                    column "Libro" do |lending_book|
                book = Book.find(lending_book.book_id)
                link_to book.title, [:admin, book]
            end
            column "Estado", :sortable => :state do |lending_book|
                if lending_book.state == 0
                    status_tag("Prestado", :red)
                elsif lending_book.state == 1
                    status_tag("En estante", :green)
                else
                    status_tag("Recepcionado", :orange)
                end
            end
            column "Fecha de préstamo", :date
                    column "Días de préstamo", :days do |lb|
                        div :class => "center-align" do
                            lb.days
                        end
                    end
                    column "Multa", :amount do |lending_book|
                        div :class => "right-align" do
                            number_to_currency(lending_book.amount, :locale => :es)
                        end
                    end
                    column "Préstamo", :state do |lending_book|
                    link_to "Ver préstamo", [:admin, lending_book]
                end
                end
            
                strong { link_to "Ver Todos", admin_lending_books_path }
            rescue Exception=>e
                table_for LendingBook.where(:reader_id => 0 ) do            
                    column "Libro" do |lending_book|
                book = Book.find(lending_book.book_id)
                link_to book.title, [:admin, book]
            end
            column "Estado", :state do |lending_book|
                if lending_book.state == 0
                    link_to "Prestado", [:admin, lending_book]
                elsif lending_book.state == 1
                    link_to "En estante", [:admin, lending_book]
                else
                    link_to "Recepcionado", [:admin, lending_book]
                end
            end
            column "Fecha de préstamo", :date
                    column "Días de préstamo", :days do |lb|
                        div :class => "center-align" do
                            lb.days
                        end
                    end
                    column "Multa" do |lending_book|
                  if lending_book.state == 0
                    #calcular dias habiles
                    days_after = 0
                    lend = lending_book.date
                    now = Time.current()
                    while now > lend
                     days_after = days_after + 1 unless now.saturday? or now.sunday?
                     now = now - 1.day
                    end

                    if days_after > lending_book.days
                      #tiene multa
                      valor = (((days_after - lending_book.days) * lending_book.amount))
                      fine = number_to_currency(valor, :locale => :es)
                      div :class => "right-align" do
                        status_tag(fine, :red)
                      end
                    else
                      if days_after > lending_book.days / 2
                        div :class => "right-align" do
                            status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :orange)
                        end
                      else
                        div :class => "right-align" do
                            status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :green)
                        end
                      end
                    end
                  else
                    div :class => "right-align" do
                      fine = lending_book.fine
                      number_to_currency(fine, :locale => :es)
                    end
                  end
                end
                end
             end
        end
            
        
    end

  end 


    
end

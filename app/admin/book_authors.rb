#encoding: utf-8
ActiveAdmin.register BookAuthor do
  config.per_page = 15
  menu :label => "Registros"
  
  #Remueve los filtros
  config.clear_sidebar_sections!

  index :download_links => false, :title => 'Registros' do
  	column "Autor", :sortable => :author_id do |book_author|
      author = Author.find(book_author.author_id)
      link_to author.name, [:admin, author]
    end
  	column "Libro", :sortable => :book_id do |book_author|
      book = Book.find(book_author.book_id)
      link_to book.title, [:admin, book]
    end
  	default_actions
  end

  form do |f|
      f.inputs "Detalles Registro" do
        f.input :author_id, :as => :select , :prompt => "- seleccione un autor -", :collection => Author.all.map { |a| [a.name, a.id] }       
        f.input :book_id,  :class => 'chzn-select', :as => :select , :prompt => "- seleccione un libro -",:collection => Book.all.map { |b| [b.title, b.id]}

      end

      f.inputs  do
	    "<div id=\"element-name\">
	      (*) Campos obligatorios
	    </div>".html_safe
  		end

      f.actions

  end

  #show
  show do |book_author|
      attributes_table do
        row :author_id
        row :book_id
      end
      #active_admin_comments
  end

end

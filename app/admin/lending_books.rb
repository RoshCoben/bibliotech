#encoding: utf-8
ActiveAdmin.register LendingBook, {:sort_order => :updated_at_desc} do

  member_action :return  do
    lending_book = LendingBook.find(params[:id])

    lending_book.update_attributes(:state => 2)

    now = Time.current()
    lending_book.update_attributes(:return => now)

    days_after = 0
    lend = lending_book.date
    while now > lend
      days_after = days_after + 1 unless now.saturday? or now.sunday?
      now = now - 1.day
    end
    if days_after > lending_book.days
     #tiene multa
      valor = (((days_after - lending_book.days) * lending_book.amount))
      lending_book.update_attributes(:fine => valor)
    end
    redirect_to ("/admin/lending_books")
  end

  config.per_page = 15
  menu :label => "Préstamos"

  filter :reader_name, :as => :string
  filter :book_title, :as => :string
  filter :days
  filter :amount

  scope :Todos, :default => true
  scope :Prestados
  scope :En_estante
  scope :Recepcionados

  index :download_links => false, :title => 'Préstamos' do
    column "Lector", :sortable => :reader_id do |lending_book|
      reader = Reader.find(lending_book.reader_id)
      link_to reader.name, [:admin, reader]
    end
    column "Libro", :sortable => :book_id do |lending_book|
      book = Book.find(lending_book.book_id)
      link_to book.title, [:admin, book]
    end
    column "Estado", :sortable => :state do |lending_book|
      if lending_book.state == 0
        status_tag("Prestado", :red)
      elsif lending_book.state == 1
        status_tag("En estante", :green)
      else
        status_tag("Recepcionado", :orange)
      end
    end
    #column :date
    column "Multa" do |lending_book|
      if lending_book.state == 0
        #calcular dias habiles
        days_after = 0
        lend = lending_book.date
        now = Time.current()
        while now > lend
         days_after = days_after + 1 unless now.saturday? or now.sunday?
         now = now - 1.day
        end

        if days_after > lending_book.days
          #tiene multa
          valor = (((days_after - lending_book.days) * lending_book.amount))
          fine = number_to_currency(valor, :locale => :es)
          div :class => "right-align" do
            status_tag(fine, :red)
          end
        else
          if days_after > lending_book.days / 2
            div :class => "right-align" do
              status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :orange)
            end
          else
            div :class => "right-align" do
              status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :green)
            end
          end
        end
      else
        div :class => "right-align" do
          fine = lending_book.fine
          number_to_currency(fine, :locale => :es)
        end
      end
    end
    if current_admin_user.role == 'admin'
      column "Entregar libro" do |lending_book|
        if lending_book.state == 0
          link_to  "Devolver", "/admin/lending_books/"+lending_book.id.to_s+"/return"
        end
      end
    end
    default_actions
  end

  #_form
  form do |f|
    f.inputs "Detalles de Préstamo" do
      f.input :book_id, :as => :select ,:prompt => "- seleccione un libro -", :collection => Book.all.map { |b| [b.title, b.id] }
      f.input :reader_id, :as => :select ,:prompt => "- seleccione un lector -", :collection => Reader.all.map { |r| [r.name, r.id] }
      f.input :date, :as => :datepicker
      f.input :state, :as => :select , :prompt => "- seleccione un estado -",:collection => [["Prestado", 0], ["En estante", 1], ["Recepcionado", 2]]
      f.input :days
      f.input :amount, :label => 'Valor multa*'
      f.input :observation
    end

    f.inputs  do
      "<div id=\"element-name\">
        (*) Campos obligatorios
      </div>".html_safe
    end

    f.actions

  end

  #show
  show do |lending_book|
      attributes_table do
        row :book_id
        row :reader_id
        row "Estado" do |lending_book|
          if lending_book.state == 0
            status_tag("Prestado", :red)
          elsif lending_book.state == 1
            status_tag("En estante", :green)
          else
            status_tag("Recepcionado", :orange)
          end
        end
        row :date
        row :days 
        row :return
        row "Valor día de atraso" do |lending_book|
          valor = lending_book.amount
          number_to_currency(valor, :locale => :es)
        end
        row "Multa" do |lending_book|
         if lending_book.state == 0
        #calcular dias habiles
        days_after = 0
        lend = lending_book.date
        now = Time.current()
        while now > lend
         days_after = days_after + 1 unless now.saturday? or now.sunday?
         now = now - 1.day
        end

        if days_after > lending_book.days
          #tiene multa
          valor = (((days_after - lending_book.days) * lending_book.amount))
          fine = number_to_currency(valor, :locale => :es)
          status_tag(fine, :red)
        else
          if days_after > lending_book.days / 2
            status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :orange)
          else
            status_tag("Quedan " + (((lending_book.days - days_after).to_i).to_s) + " días", :green)
          end
        end
        else
          fine = lending_book.fine
          number_to_currency(fine, :locale => :es)
      end
        end        
        row :observation
      end
      #active_admin_comments
  end

end

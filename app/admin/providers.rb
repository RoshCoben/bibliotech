#encoding: utf-8
ActiveAdmin.register Provider do
  config.per_page = 15
  menu :label => "Proveedores"

  filter :name

 index :download_links => false, :title => 'Proveedores' do
  	column :name, :sortable => :name do |provider|
  		div :class => "right-align" do
  			provider.name
  		end
  	end
  	column :address
    column :phone
    column "E-Mail", :sortable => :email do |provider|
      link_to provider.email, "mailto:"+provider.email
    end
  	default_actions
  end  

  #_form
  form do |f|
    f.inputs "Detalles de Proveedor" do
        f.input :name
        f.input :address
        f.input :phone
        f.input :email

    end

    f.inputs  do
      "<div id=\"element-name\">
        (*) Campos obligatorios
      </div>".html_safe
    end

    f.actions

  end

  #show
  show do |provider|
      attributes_table do
        row :name
        row :address
        row :phone
        row "E-mail" do |provider|
          link_to provider.email, "mailto:"+provider.email
        end
      end
      #active_admin_comments
  end
  
end

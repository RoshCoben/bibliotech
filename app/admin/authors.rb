ActiveAdmin.register Author do
  config.per_page = 15
  #controller do
   # authorize_resource
    
   # include ActiveAdminCanCan

  #end

  menu :label => "Autores"
  filter :name
  filter :nationality

  #index
  index :download_links => false, :title => 'Autores' do
    column :name
    column :nationality
    default_actions
  end

  #_form
  form do |f|
    f.inputs "Detalles de Autor" do
      f.input :name
      f.input :nationality
    end
    f.inputs  do
      "<div id=\"element-name\">
        (*) Campos obligatorios
      </div>".html_safe
    end
    f.actions
  end

  #show
  show do |author|
      attributes_table do
        row :name
        row :nationality
      end

      q = BookAuthor.where(:author_id => author.id).length
      if q>0 
        table_for BookAuthor.where(:author_id => author.id) do |book_author|
          column "Libro(s)" do |book_author|
            book = Book.find(book_author.book_id)
            link_to book.title, [:admin, book]
          end
        end
      end
      #active_admin_comments
  end
  
end

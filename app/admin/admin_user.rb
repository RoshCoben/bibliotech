#encoding: utf-8
ActiveAdmin.register AdminUser do  
  config.per_page = 15
  filter :email
  menu :label => "Usuarios"   
  index :download_links => false, :title => 'Usuarios' do
    column "E-mail", :sortable => :email do |user|
      link_to user.email, "mailto:"+user.email
    end                     
    column "Ingreso actual", :current_sign_in_at        
    column "Ingreso anterior", :last_sign_in_at  
    column :sign_in_count do |admin_user|
      div :class => "right-align" do
        admin_user.sign_in_count
      end
    end
    default_actions                      
  end                                 

  form do |f|                         
    f.inputs "Detalles" do       
      f.input :email                  
      f.input :password               
      f.input :password_confirmation 
      if current_admin_user.role == 'admin' #is an admin
        f.input :role, :as => :select , :prompt => "- Seleccione un rol -",:collection => [["Administrador", 'admin'], ["Lector", 'reader']]
      end
    end                               
    f.actions                         
  end 

  #show
  show do |admin_user|
      attributes_table do
        row "E-mail" do |user|
          link_to user.email, "mailto:"+user.email
        end   
        row :sign_in_count
        row :current_sign_in_at
        row :last_sign_in_at
        #row :current_sign_in_ip
        #row :last_sign_in_ip
        row "Rol" do |user|
          if user.role == 'reader'
            user.role = 'Lector'
          else
            user.role = 'Administrador'
          end
        end
      end
      #active_admin_comments
  end
  

end                                   

#encoding: utf-8
ActiveAdmin.register Reader do
  config.per_page = 15
  menu :label => "Lectores"
  
  filter :name
  filter :address
  filter :phone
  filter :email
  filter :observation

  index :download_links => false, :title => 'Lectores' do
    column :name
  	column :"address"
    column "E-mail", :sortable => :email do |reader|
      link_to reader.email, "mailto:"+reader.email
    end
  	default_actions
  end

  #_form
  form do |f|
    f.inputs "Detalles de Lector" do
      f.input :name 
      f.input :address
      f.input :phone
      f.input :email
      f.input :observation
    end

    f.inputs  do
      "<div id=\"element-name\">
        (*) Campos obligatorios
      </div>".html_safe
    end
    f.actions
  end  

  #show
  show do |reader|
      attributes_table do
        row :name
        row :address
        row :observation
        row "E-mail" do |reader|
          link_to reader.email, "mailto:"+reader.email
        end
        row :phone
      end
      #active_admin_comments
  end

end

//= require active_admin/base
//= require 'i18n/jquery.ui.datepicker-es.js'

$(function(){
	$('.datepicker').on('focus', function(){
		$(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
		//$(this).datepicker({ navigationAsDateFormat: true });
		//$(this).datepicker({ prevText: "Earlier" });
		//$(this).datepicker({ nextText: "Later" });
	});
});

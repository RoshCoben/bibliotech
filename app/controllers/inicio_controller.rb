class InicioController < ApplicationController
	
  def index
    @noticias = News.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @noticias }
    end
  end
end

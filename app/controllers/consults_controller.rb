class ConsultsController < InheritedResources::Base



def create
  @consultation = Consultation.new(params[:consultation])

  respond_to do |format|
  if @consultation.save
  ConsultationMailer.registration_confirmation(@consultation).deliver
  format.html { redirect_to(@consultation, :notice => 'User was successfully created.') }
  format.xml  { render :xml => @consultation, :status => :created, :location => @consultation }
  else
  format.html { render :action => "new" }
  format.xml  { render :xml => @consultation.errors, :status => :unprocessable_entity }
  end
  end
  end



end

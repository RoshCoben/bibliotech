class ConsultationsController < InheritedResources::Base

  def index
    @consultations = Consultation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @consultations }
    end
  end

  def new
    @consultation = Consultation.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @consultation }
    end
  end

def create
  @consultation = Consultation.new(params[:consultation])

  respond_to do |format|
  if @consultation.save
  ConsultationMailer.menssage(@consultation).deliver
  format.html { redirect_to(@consultation, :notice => 'La Consulta se ha enviado con exito.') }
  format.xml  { render :xml => @consultation, :status => :created, :location => @consultation }
  else
  format.html { render :action => "new" }
  format.xml  { render :xml => @consultation.errors, :status => :unprocessable_entity }
  end
  end
  end

  def show
    @consultation = Consultation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @consultation }
    end
  end  



end

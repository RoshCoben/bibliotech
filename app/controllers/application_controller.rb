class ApplicationController < ActionController::Base
  protect_from_forgery
  
  # Override build_footer method in ActiveAdmin::Views::Pages
  require 'active_admin_views_pages_base.rb'


  #rescue_from CanCan::AccessDenied do |exception|
  	#flash[:error] = "Acceso denegado."
  	#redirect_to root_url    
  #end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  def after_sign_out_path_for(resource_or_scope)
    "/"
  end

  #def authenticate_admin_user!
  #  redirect_to new_admin_user_session_path unless current_admin_user.role == 'admin'
  #end
end

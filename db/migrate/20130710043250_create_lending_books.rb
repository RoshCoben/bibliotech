class CreateLendingBooks < ActiveRecord::Migration
  def change

    create_table :books do |t|
      t.integer :code
      t.string :title
      t.integer :edition
      t.string :description
      t.integer :quantity
      t.string :observation
      t.timestamps
    end

    create_table :lending_books do |t|
      t.belongs_to :book
      t.belongs_to :reader
      t.integer :book_id
      t.integer :state
      t.integer :days
      t.float :amount
      t.float :fine, :default => 0
      t.datetime :date
      t.integer :reader_id
      t.string :observation
      t.timestamps
    end
  end
end

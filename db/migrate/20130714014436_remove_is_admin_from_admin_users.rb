class RemoveIsAdminFromAdminUsers < ActiveRecord::Migration
  def up
    remove_column :admin_users, :is_admin
  end

  def down
    add_column :admin_users, :is_admin, :string
  end
end

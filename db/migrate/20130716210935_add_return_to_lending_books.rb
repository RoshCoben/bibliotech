class AddReturnToLendingBooks < ActiveRecord::Migration
  def change
    add_column :lending_books, :return, :datetime
  end
end

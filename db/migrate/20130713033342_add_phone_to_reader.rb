class AddPhoneToReader < ActiveRecord::Migration
  def change
    add_column :readers, :phone, :string
  end
end

class CreateConsults < ActiveRecord::Migration
  def change
    create_table :consults do |t|
      t.string :name
      t.text :comment

      t.timestamps
    end
  end
end

class CreateConsultations < ActiveRecord::Migration
  def change
    create_table :consultations do |t|
      t.string :subject
      t.string :name
      t.string :surname
      t.string :email
      t.text :comment

      t.timestamps
    end
  end
end

class AddProviderIdToBook < ActiveRecord::Migration
  def change
    add_column :books, :provider_id, :integer
  end
end
